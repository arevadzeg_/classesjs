class Character {
  name = "";
  role = "";
  armor = 0;
  damage = 0;

  constructor(name, role, armor, damage) {
    this.name = name;
    this.role = role;
    this.armor = armor;
    this.damage = damage;
  }

  tellArmor() {
    console.log(`I have ${this.armor} armor`);
  }

  tellDamage() {
    console.log(
      `I have ${this.damage} ${
        this.role == "Mage" || this.role == "Support"
          ? "magic power"
          : "attack damage"
      }`
    );
  }
}

class Mage extends Character {
  canFly = true;
  magicPower = 0;

  constructor(name, armor, damage, magicPower) {
    super(name, "Mage", armor, damage);
    this.magicPower = magicPower;
  }

  fly() {
    console.log("I can Fly");
  }
}

class Support extends Mage {
  heal = 0;
  role = "Support";

  constructor(name, armor, damage, magicPower, heal) {
    super(name, armor, damage, magicPower);
    this.heal = heal;
  }

  canHeal() {
    console.log(`I can heal ${this.heal} HP`);
  }
}

class Fighter extends Character {
  attackdamage = 0;
  range = 0;

  constructor(name, role, armor, damage, range, attackdamage) {
    super(name, role, armor, damage);
    this.range = range;
    this.attackdamage = attackdamage;
  }

  checkRange() {
    console.log(
      `I am ${this.range > 30 ? "long range" : "short range"} fighter`
    );
  }
}

const adc = new Character("Gio", "mage", 50, 50);
const mage1 = new Mage("MageName", 50, 50, 75);
const support1 = new Support("MidOrFeed", 50, 50, 105, 85);
const fighterboi = new Fighter("Fizz", "Tank", 25, 55, 40, 102);

adc.tellDamage();
mage1.tellDamage();
support1.tellDamage();
fighterboi.checkRange();
console.log(adc);
console.log(mage1);
console.log(support1);
console.log(fighterboi);
